package com.jiomitwa.yaml;

import java.util.Map;

public final class Config2 {

	private Map< String, String > propDetails;

	public Map<String, String> getPropDetails() {
		return propDetails;
	}

	public void setPropDetails(Map<String, String> propDetails) {
		this.propDetails = propDetails;
	} 
	
	@Override
    public String toString() {
		return propDetails.toString();
    }
}
