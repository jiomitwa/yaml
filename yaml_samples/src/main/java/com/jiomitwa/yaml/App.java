package com.jiomitwa.yaml;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.yaml.snakeyaml.Yaml;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		
		Yaml yaml = new Yaml();
		try {
			InputStream in = Files
					.newInputStream(Paths.get("C:\\netapp_work/workarea_personal/jioMitwa_ws/yaml/properties.yaml"));
			Configuration config = yaml.loadAs(in, Configuration.class);
			System.out.println(config.toString());
		} catch (Exception e) {
			System.out.println(e);
		}

		try {
			InputStream in = Files
					.newInputStream(Paths.get("C:\\netapp_work/workarea_personal/jioMitwa_ws/yaml/prop.yaml"));
			Config2 config = yaml.loadAs(in, Config2.class);
			System.out.println(config.toString());
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
