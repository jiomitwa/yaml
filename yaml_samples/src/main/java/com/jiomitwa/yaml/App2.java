package com.jiomitwa.yaml;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import org.yaml.snakeyaml.Yaml;

/**
 * Hello world!
 *
 */
public class App2 {
	public static void main(String[] args) {
		
		Yaml yaml = new Yaml();
		try {
			InputStream in = Files
					.newInputStream(Paths.get("C:\\netapp_work/workarea_personal/jioMitwa_ws/yaml/prop.yaml"));
			MyProp prop = yaml.loadAs(in, MyProp.class);
			System.out.println(prop.toString());
			System.out.println(prop.users);
			
			Properties p = prop.oldProp;
			System.out.println(p.getProperty("MONGO_HOST_QA"));
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
